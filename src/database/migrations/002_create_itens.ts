import Knex from 'knex';

export async function up(knex: Knex){
    return await knex.schema.createTable('items', function (table) {
        table.bigIncrements('id').primary();
        table.string('image').notNullable();
        table.string('title').notNullable();
     });

};


export async function down(knex: Knex, Promise: any){
    return await knex.schema.dropTableIfExists('items');
};