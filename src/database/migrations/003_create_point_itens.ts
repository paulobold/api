import Knex from 'knex';

export async function up(knex: Knex){
    return await knex.schema.createTable('point_items', function (table) {
        table.bigIncrements('id').primary();
        table.integer('point_id').notNullable().references('id').inTable('points');
        table.integer('item_id').notNullable().references('id').inTable('items');
     });

};


export async function down(knex: Knex, Promise: any){
    return await knex.schema.dropTableIfExists('point_items');
};